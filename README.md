
## Init Project

```
git clone https://ivinnic@bitbucket.org/ivinnic/text-to-speech.git
```


В папке проекта

```
composer install
```

- Создать сервис аккаунт в google cloud.
- Скачать ключ доступа.
- Поместить в папку проекта под именем key.json


Запуск

```
php text-to-spech.php
```

На выходе будет файл output.mp3