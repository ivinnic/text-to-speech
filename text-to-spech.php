<?php

putenv("GOOGLE_APPLICATION_CREDENTIALS=".__DIR__."/key.json");
# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;

/** Uncomment and populate these variables in your code */
 $text = "
In the summer, many large construction firms and their subcontractors are busy working on schools and colleges, which can't have major works carried out during the academic year. Yet it's also frequently the time that the public wants work done. If you're not on the public sector frameworks, take advantage of the fact that many firms are tied up during the summer and plan a marketing campaign that will bring in work over the summer months.
";

// create client object
$client = new TextToSpeechClient();

$input_text = (new SynthesisInput())
    ->setText($text);

// note: the voice can also be specified by name
// names of voices can be retrieved with $client->listVoices()


$voice = (new VoiceSelectionParams())
    ->setLanguageCode('en-US')
//	->setName('en-GB-WaveNet-B')
    ->setSsmlGender(SsmlVoiceGender::MALE)
;

$audioConfig = (new AudioConfig())
    ->setAudioEncoding(AudioEncoding::MP3);

$response = $client->synthesizeSpeech($input_text, $voice, $audioConfig);
$audioContent = $response->getAudioContent();

file_put_contents('output.mp3', $audioContent);
print('Audio content written to "output.mp3"' . PHP_EOL);

$client->close();